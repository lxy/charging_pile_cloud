package com.suda.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suda.platform.entity.AdminUserRole;

/**
 * <p>
 * 角色用户关联表 Mapper 接口
 * </p>
 */
public interface AdminUserRoleMapper extends BaseMapper<AdminUserRole> {

}

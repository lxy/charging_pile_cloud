package com.suda.platform.service;

/**
 * @package com.suda.server.service
 */
public interface ICommonService {
    /**
     * 活动商品倒计时结束 设置中奖人员
     *
     * @param id 活动商品id
     */
    void overPrizeCountdown(Long id);

    /**
     * 验证码验证
     *
     * @param account
     * @param code
     */
    void checkTelToken(String account, String code);

}

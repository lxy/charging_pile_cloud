package com.suda.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suda.platform.entity.AdminUserRole;

/**
 * <p>
 * 角色用户关联表 服务类
 * </p>
 */
public interface IAdminUserRoleService extends IService<AdminUserRole> {

}
